<!DOCTYPE html>
<html>
<head>
	<title>Inventory Manager Home Page</title>
	<!-- Compiled and minified CSS -->
  	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.98.1/css/materialize.min.css">
  	<!--<script src="https://use.fontawesome.com/7d61ae0bda.js"></script>-->
  	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
  <!-- Compiled and minified JavaScript -->
  	<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.98.1/js/materialize.min.js"></script>
          
</head>
<body>
	<!-- header-->
	<nav class="teal lighten-2">
	    <div class="nav-wrapper" style="padding:0px 10px 0px 10px">
	      <a href="/" class="brand-logo">Inventory Manager</a>
	      <a href="#" data-activates="mobile-demo" class="button-collapse"><i class="material-icons">menu</i></a>
	      <ul class="right hide-on-med-and-down">
	        <li><a href="#">Devashish Manjhi</a></li>
	        <li><a class="waves-effect waves-light btn" href="#changePasswordModal">Change Password</a></li>
	        <li><a class="modal-trigger waves-effect waves-light btn" href="#logoutModal"><i class="material-icons">power_settings_new</i></a></li>
	      </ul>
	      <ul class="side-nav" id="mobile-demo">
	        <li><a href="#">Devashish Manjhi</a></li>
	        <li><a class="waves-effect waves-light btn" href="#changePasswordModal">Change Password</a></li>
	        <li><a class="modal-trigger waves-effect waves-light btn" href="#logoutModal"><i class="material-icons" style="margin-right: -15px">power_settings_new</i>Logout</a></li>
	      </ul>
	    </div>
  	</nav>
	<div class="container">
		<h3 style="text-align: center">Blood Component Record</h3>
		<?php if(isset($messageInsert) && ($messageInsert!=null)) { ?>
					<div style="color: green;text-align:center; padding: 10px">
						<p><?= $messageSearch?></p>
						<p><?= $messageInsert?></p>
						<p><?= $messageUpdate?></p>
					</div>
				<?php } ?>
		<div class="row">
			 	<form method="get" action="/submitBloodComponentForm" class="col s8 offset-s2">
			      <div class="row">
				        <div class="input-field col s6">
				          <input placeholder="Placeholder" id="date" name="date" type="date" class="validate">
				        </div>
				        <div class="input-field col s6">
				          <input id="sampleID" name="sampleID" type="text" class="validate">
				          <label for="sampleID">Sample ID</label>
				        </div>
			      </div>
			      <div class="row">
				        <div class="input-field col s6">
				          <input id="wb" name="wb" type="number" class="validate">
				          <label for="wb">Amount of WBC</label>
				        </div>
				        <div class="input-field col s6">
				          <input id="pc" name="pc" type="number" class="validate">
				          <label for="pc">Amount of PC</label>
				        </div>
			      </div>
			      <div class="row">
				        <div class="input-field col s6">
				          <input id="ffp" name="ffp" type="number" class="validate">
				          <label for="ffp">Amount of FFP</label>
				        </div>			      
				        <div class="input-field col s6">
				          <input id="paedFfp" name="paedFfp" type="number" class="validate">
				          <label for="paedFfp">Amount of paed FFP</label>
				        </div>
			      </div>
			      <div class="row">
				        <div class="input-field col s6">
				          <input id="plateletCone" name="plateletCone" type="number" class="validate">
				          <label for="plateletCone">Amount of platelet cone</label>
				        </div>			      
				        <div class="input-field col s6">
				          <input id="cryoPpt" name="cryoPpt" type="number" class="validate">
				          <label for="cryoPpt">Amount of cryo ppt</label>
				        </div>
			      </div>
			      <div class="row">
				        <div class="input-field col s6">
				          <input id="cfp" name="cfp" type="number" class="validate">
				          <label for="cfp">Amount of CFP</label>
				        </div>
				  </div>
				  <div class="row">
				        <div class="input-field col s12 center-align">
				          <input id="add" name="add" type="submit" value="add" class="validate">
				        </div>
			      </div>
			    </form>
		</div>
	</div>
	<div id="logoutModal" class="modal modal-fixed-footer red lighten-4" style="height: 200px">
	    <div class="modal-content">
	      <h4>Logout Action</h4>
	      <p>Are you sure you want to logout?</p>
	    </div>
	    <div class="modal-footer red accent-1">
	      <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat ">Agree</a>
	       <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat ">Disagree</a>
	    </div>
	</div>
	<div id="changePasswordModal" class="modal modal-fixed-footer teal lighten-4" style="height: 400px">
	    <div class="modal-content">
	      <h4>Change Password</h4>
	      <p>New password: <input type="password" name="newPass"></p>
	      <p>Retype password: <input type="password" name="rtyPass"></p>
	    </div>
	    <div class="modal-footer teal accent-1">
	      <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat ">Change</a>
	       <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat ">Cancel</a>
	    </div>
	</div>
</body>
<script type="text/javascript">
	$( document ).ready(function(){
		$(".button-collapse").sideNav();
		$('.modal').modal();
	})
	
</script>
</html>