<!DOCTYPE html>
<html>
<head>
	<title>Inventory Manager Home Page</title>
	<!-- Compiled and minified CSS -->
  	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.98.1/css/materialize.min.css">
  	<!--<script src="https://use.fontawesome.com/7d61ae0bda.js"></script>-->
  	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
  <!-- Compiled and minified JavaScript -->
  	<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.98.1/js/materialize.min.js"></script>
          
</head>
<body>
	<!-- header-->
	<nav class="teal lighten-2">
	    <div class="nav-wrapper" style="padding:0px 10px 0px 10px">
	      <a href="/" class="brand-logo">Inventory Manager</a>
	      <a href="#" data-activates="mobile-demo" class="button-collapse"><i class="material-icons">menu</i></a>
	      <ul class="right hide-on-med-and-down">
	        <li><a href="#">Devashish Manjhi</a></li>
	        <li><a class="waves-effect waves-light btn" href="#changePasswordModal">Change Password</a></li>
	        <li><a class="modal-trigger waves-effect waves-light btn" href="#logoutModal"><i class="material-icons">power_settings_new</i></a></li>
	      </ul>
	      <ul class="side-nav" id="mobile-demo">
	        <li><a href="#">Devashish Manjhi</a></li>
	        <li><a class="waves-effect waves-light btn" href="#changePasswordModal">Change Password</a></li>
	        <li><a class="modal-trigger waves-effect waves-light btn" href="#logoutModal"><i class="material-icons" style="margin-right: -15px">power_settings_new</i>Logout</a></li>
	      </ul>
	    </div>
  	</nav>
	<div class="container">
		<div class="row" style="margin-top: 100px">
			<div class="col l4"><div class="card">
			    <div class="card-image waves-effect waves-block waves-light">
			      <img class="activator" src="../images/inventory.png" style="height: 175px;">
			    </div>
			    <div class="card-content">
			      <span class="card-title activator grey-text text-darken-4"><a href="/inventoryRecord">View/Update Inventory Database</a><i class="material-icons right">more_vert</i></span>
			    </div>
			    <div class="card-reveal">
			      <span class="card-title grey-text text-darken-4"><a href="#">Inventory Database</a><i class="material-icons right">close</i></span>
			      <p>The inventory maintains a record of the amount of W.B.C., P.C., F.F.P., Paed F.F.P., Platelet Cone, Cryo PPt, C.F.P. and remarks. </p>
			    </div>
			</div></div>
			<div class="col l4">
				<div class="card">
				    <div class="card-image waves-effect waves-block waves-light">
				      <img class="activator" src="../images/bloodComponents.jpg" style="height: 175px">
				    </div>
				    <div class="card-content">
				      <span class="card-title activator grey-text text-darken-4"><a href='/bloodComponentForm'>Add Blood Component Record</a><i class="material-icons right">more_vert</i></span>
				    </div>
				    <div class="card-reveal">
				      <span class="card-title grey-text text-darken-4"><a href="#">Blood Component Record</a><i class="material-icons right">close</i></span>
				      <p>The Blood Component Record provides you with a form to record the amount of W.B.C., P.C., F.F.P., Paed F.F.P., Platelet Cone, Cryo PPt, C.F.P. in each sample.</p>
				    </div>
				</div>
			</div>
			<div class="col l4">
				<div class="card">
				    <div class="card-image waves-effect waves-block waves-light">
				      <img class="activator" src="../images/bloodComponents.jpg" style="height: 175px">
				    </div>
				    <div class="card-content">
				      <span class="card-title activator grey-text text-darken-4"><a href='/bloodComponentRecord'>View/Update Blood Component Record</a><i class="material-icons right">more_vert</i></span>
				    </div>
				    <div class="card-reveal">
				      <span class="card-title grey-text text-darken-4"><a href="#">Blood Component Database</a><i class="material-icons right">close</i></span>
				      <p>The Blood Component Database contains a comprehensive record of the amount of W.B.C., P.C., F.F.P., Paed F.F.P., Platelet Cone, Cryo PPt, C.F.P. with respect to each sample ID.</p>
				    </div>
				</div>
			</div>
			<div class="col l4"><div class="card">
			    <div class="card-image waves-effect waves-block waves-light">
			      <img class="activator" src="../images/stock.jpg" style="height: 175px">
			    </div>
			    <div class="card-content">
			      <span class="card-title activator grey-text text-darken-4"><a href="#">View/Update Stock Record</a><i class="material-icons right">more_vert</i></span>
			    </div>
			    <div class="card-reveal">
			      <span class="card-title grey-text text-darken-4"><a href="#">Stock Record</a><i class="material-icons right">close</i></span>
			      <p>The stock records encapsulates the number of Type A, Type B and Type C bags present and the amount of reagents present.</p>
			    </div>
			</div></div>
		</div>
	</div>
	<div id="logoutModal" class="modal modal-fixed-footer red lighten-4" style="height: 200px">
	    <div class="modal-content">
	      <h4>Logout Action</h4>
	      <p>Are you sure you want to logout?</p>
	    </div>
	    <div class="modal-footer red accent-1">
	      <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat ">Agree</a>
	       <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat ">Disagree</a>
	    </div>
	</div>
	<div id="changePasswordModal" class="modal modal-fixed-footer teal lighten-4" style="height: 400px">
	    <div class="modal-content">
	      <h4>Change Password</h4>
	      <p>New password: <input type="password" name="newPass"></p>
	      <p>Retype password: <input type="password" name="rtyPass"></p>
	    </div>
	    <div class="modal-footer teal accent-1">
	      <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat ">Change</a>
	       <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat ">Cancel</a>
	    </div>
	</div>
</body>
<script type="text/javascript">
	$( document ).ready(function(){
		$(".button-collapse").sideNav();
		$('.modal').modal();
	})
	
</script>
</html>