<?php

//a class designed in the singleton pattern to establish a connection with the mySQL database and handle all database queries
class DbManager {
	private static $db_manager;
	private static $connection;
	private static $servername = "localhost";
	private static $username = "root";
	private static $password = "";
	private static $dbname = "bhorukabloodbank";

	private function __construct(){
		self::$connection = $this->createConnection();
		$this->setupDb();
	}

	//a method which returns the singleton instance of this class
	public static function getInstance(){
		if (!isset(self::$instance)) {
		    self::$db_manager = new DbManager();
		}
		return self::$db_manager;
	}
	
	//a method which establishes a database connection
	private function createConnection(){
		try {
		    $conn = new PDO("mysql:host=".self::$servername.";", self::$username, self::$password);
		    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		    return $conn;
		}
		catch(PDOException $e) {
		    echo "Error: " . $e->getMessage();
		}
	}
	
	//a method which sets up the database structure
	private function setupDb(){
		try{
	    	self::$connection->exec("USE ". self::$dbname);
		}
		catch(PDOException $e) {
		    echo "Error: " . $e->getMessage();
		}	
	}
	
	//I tried to overload the function and it can be done, but PHP has a really shitty way of doing it so I left that part out for the sake of simplicity

	//a method which provides a wrapper around select queries. Accepts strings as parameters.
	public function select($fields, $tables){
		$sql = "SELECT $fields FROM $tables";
		$stmt = self::$connection->prepare($sql);
		$stmt->execute();
		$result = $stmt->setFetchMode(PDO::FETCH_ASSOC);
		$result  = $stmt->fetchAll();
		return $result;
	}
	

	public function selectWhere($fields, $tables, $where){
		$sql = "SELECT $fields FROM $tables WHERE $where";
		$stmt = self::$connection->prepare($sql);
		$stmt->execute();
		$result = $stmt->setFetchMode(PDO::FETCH_ASSOC);
		$result  = $stmt->fetchAll();
		return $result;
	}

	public function insert($fields,$table,$values){
		$sql = "INSERT INTO $table($fields) VALUES $values";
		$stmt = self::$connection->prepare($sql);
		$message=$stmt->execute();
		if($message=='1')
			$message='Successful Insertion';
		else
			$message='Error during Insertion';
		return $message;
	}

	public function update($table,$colAndVal,$condition){
		$sql = "UPDATE $table SET $colAndVal WHERE $condition";
		$stmt = self::$connection->prepare($sql);
		$message=$stmt->execute();
		if($message=='1')
			$message='Successful Updation';
		else
			$message='Error during Updation';
		return $message;
	}

	public function delete(){

	}
}