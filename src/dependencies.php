<?php
// DIC configuration

$container = $app->getContainer();

// view renderer
$container['renderer'] = function ($c) {
    $settings = $c->get('settings')['renderer'];
    return new Slim\Views\PhpRenderer($settings['template_path']);
};

//add all managers here to make them accesible from the instance of the Slim app
// Database manager
$container['dbManager'] = DbManager::getInstance();