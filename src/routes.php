<?php

$app->get('/', function ($request, $response, $args) {
	return $this->renderer->render($response, 'InventoryManager/inventoryManagerHome.php', $args);
});

$app->get('/bloodComponentForm', function ($request, $response, $args) {
   	// Render index view
   	//return print(json_encode($args));
	return $this->renderer->render($response, 'InventoryManager/inventoryManagerBloodComponentForm.php', $args);
});

$app->get('/bloodComponentRecord', function ($request, $response, $args) {
	$args['componentDB'] = $this->dbManager->select("*", "blood_component_database");
	return $this->renderer->render($response, 'InventoryManager/inventoryManagerBloodComponentRecord.php', $args);
});

$app->get('/submitBloodComponentForm', function ($request, $response, $args) {
	$Sample_Id=$_GET['sampleID'];
	$checkSampleID=$this->dbManager->selectWhere("Sample_Id,Blood_Group","sample_database","Sample_Id='$Sample_Id'");
	if($checkSampleID[0]['Sample_Id']==$Sample_Id)
	{
		$date=$_GET['date'];
		$WB=(int)$_GET['wb'];
		$PC=(int)$_GET['pc'];
		$FFP=(int)$_GET['ffp'];
		$Paed_FFP=(int)$_GET['paedFfp'];
		$Platelet_Cone=(int)$_GET['plateletCone'];
		$Cryo_Ppt=(int)$_GET['cryoPpt'];
		$CFP=(int)$_GET['cfp'];
		$Blood_Group=$checkSampleID[0]['Blood_Group'];
		$args['messageSearch']="SampleID ".$Sample_Id." has been found in the Sample Database with ".$Blood_Group." as the blood group";
		$args['messageInsert'] = $this->dbManager->insert("Date,Sample_Id,WB,PC,FFP,Paed_FFP,Platelet_Cone,Cryo_Ppt,CFP", "blood_component_database","('$date','$Sample_Id','$WB','$PC','$FFP','$Paed_FFP','$Platelet_Cone','$Cryo_Ppt','$CFP')");
		$args['messageUpdate']= $this->dbManager->update("inventory_database","WB=WB+$WB,PC=PC+$PC,FFP=FFP+$FFP,Paed_FFP=Paed_FFP+$Paed_FFP,Platelet_Cone=Platelet_Cone+$Platelet_Cone,Cryo_Ppt=Cryo_Ppt+$Cryo_Ppt,CFP=CFP+$CFP","Blood_Group='$Blood_Group'");
	}
	else
	{
		$args['messageSearch']="SampleID ".$Sample_Id." has not been found in the Sample Database. Hence insertion has been rejected.";
		$args['messageInsert']="Failed Insertion to Blood Component Database";
		$args['messageUpdate']= "Failed Updation to Inventory Database";
	}
	return $this->renderer->render($response, 'InventoryManager/inventoryManagerBloodComponentForm.php', $args);
});

$app->get('/updateBloodComponentRecord', function ($request, $response, $args) {
	$Sample_Id=$_GET['sampleID'];
		$date=$_GET['date'];
		$WB=(int)$_GET['wb'];
		$PC=(int)$_GET['pc'];
		$FFP=(int)$_GET['ffp'];
		$Paed_FFP=(int)$_GET['paedFfp'];
		$Platelet_Cone=(int)$_GET['plateletCone'];
		$Cryo_Ppt=(int)$_GET['cryoPpt'];
		$CFP=(int)$_GET['cfp'];
		$Blood_Group=$checkSampleID[0]['Blood_Group'];
		//incomplete function
	
	return $this->renderer->render($response, 'InventoryManager/inventoryManagerBloodComponentRecord.php', $args);
});

$app->get('/inventoryRecord', function ($request, $response, $args) {
   	$args['inventory'] = $this->dbManager->select("*", "inventory_database");
	return $this->renderer->render($response, 'InventoryManager/inventoryManagerInventoryRecord.php', $args);
});